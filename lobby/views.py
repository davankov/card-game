import aiohttp_jinja2
from aiohttp_session import get_session
from aiohttp import web
from auth.models import User
from settings import log
import asyncio

def redirect(request, router_name):
    url = request.app.router[router_name].url()
    raise web.HTTPFound(url)


class Lobby(web.View):

    @aiohttp_jinja2.template('lobby/index.html')
    @asyncio.coroutine
    def get(self):
        session = yield from get_session(self.request)
        user_id = session.get('user')
        log.debug("user id: {}".format(user_id))
        if user_id is None:
            return {"loggedIn": False}
        user = yield from User.get_user(self.request.db, user_id)
        log.debug("user: {}".format(user))
        if isinstance(user, dict):
            # user was found, everything is OK
            return {'user': user['login'], "loggedIn": True}
        else:
            # something went wrong
            return {"loggedIn": False}
