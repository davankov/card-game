from game.views import WebSocket, GameView
from lobby.views import Lobby
from auth.views import Register, LogIn, LogOut

routes = [
    ('GET', '/',         Lobby,     'main'),
    ('GET', '/game',     GameView,  'game'),
    ('GET', '/ws',       WebSocket, 'ws'),
    ('*',   '/register', Register,  'register'),
    ('*',   '/logout',   LogOut,    'logout'),
    ('*',   '/login',    LogIn,     'login'),
]