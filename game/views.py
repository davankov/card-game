import json
import asyncio

import aiohttp_jinja2
from game.models import *
from aiohttp import web, MsgType
from aiohttp_session import get_session
from settings import log


def redirect(request, router_name):
    url = request.app.router[router_name].url()
    raise web.HTTPFound(url)


class GameView(web.View):
    @aiohttp_jinja2.template('game/index.html')
    @asyncio.coroutine
    def get(self):
        session = yield from get_session(self.request)
        if not session.get('user'):
            redirect(self.request, 'register')
        return

class WebSocket(web.View):
    
    games = {}
    turn = 1

    # start round function
    def start_round(self, game):
        log.info("Round started")
        cards0 = game.player[0].cards
        cards1 = game.player[1].cards

        self.send_message(game.player[0].websocket, {"StartRound": True, "id": 0, "Turn": 1, "card0": cards0[0].params, \
                                                     "card1": cards0[1].params, "card2": cards0[2].params, \
                                                     "card3": cards0[3].params, "card4": cards0[4].params, \
                                                     "money": game.player[0].money})
        self.send_message(game.player[1].websocket, {"StartRound": True, "id": 1, "Turn": 1, "card0": cards1[0].params, \
                                                     "card1": cards1[1].params, "card2": cards1[2].params, \
                                                     "card3": cards1[3].params, "card4": cards1[4].params, \
                                                     "money": game.player[1].money})

    def next_turn(self, game):
        log.info("Next turn")
        self.send_message(game.player[0].websocket, {"Turn": self.turn})
        self.send_message(game.player[1].websocket, {"Turn": self.turn})

    # функция отправки сообщения
    def send_message(self, ws, message):
        log.debug("send message")
        log.debug(json.dumps(message))
        ws.send_str(json.dumps(message))

    # функция отправления команд
    def execute_command(self, game, data):
        log.debug("data: {}".format(data))
        card_id = data.get("card")

        player_number = data.get("id")

        attacker = data.get("attacker")

        defender = data.get("defender")

        attack = data.get("attack")

        command = data["command"]

        if game.player[0 if player_number == 1 else 1] is None:
            return
        enemy_id = 0 if player_number == 1 else 1
        enemy = game.player[enemy_id].websocket

        if command == "LookingAtCard":
            log.info("{} player look at the {} card".format(player_number, card_id))
            self.send_message(enemy, {"EnemyLookingAtCard": True, 'card': card_id})
        elif command == "StopLookingAtCard":
            log.info("{} player stop looking at the {} card".format(player_number, card_id))
            self.send_message(enemy, {"EnemyStopLookingAtCard": True, 'card': card_id})
        elif command == "CardToField":
            log.info("{} player put card {} on the table".format(player_number, card_id))
            print(game.player[enemy_id].cards)
            print(game.player[player_number].cards)
            self.send_message(enemy, {"EnemyCardToField": True, 'card': game.player[player_number].cards[card_id - 5].params, 'card_id': card_id})
        elif command == "CardToTrash":
            log.info("{} player put card {} to trash".format(player_number, card_id))
            self.send_message(enemy, {"EnemyCardToTrash": True, 'card': card_id})
        elif command == "EndTurn":
            self.turn = data.get("turn")
            self.next_turn(game)
        elif command == "Attack":
            log.info("{} player attack enemy card {} via card {}".format(player_number, defender, attacker))
            self.send_message(enemy, {"EnemyAttack": True, 'defender': defender, "attacker": attacker, "attack": attack})
        elif command == "AtcivateCard":
            #self.send_message(enemy,)
            log.info("active")
        elif command == "DeactivateCard":
            #self.send_message(enemy,)
            log.info("deactive")

        else:
            return

    @asyncio.coroutine
    def get(self):
        ws = web.WebSocketResponse()
        yield from ws.prepare(self.request)

        # save it to app variable for correct closing program
        self.request.app['websockets'].append(ws)

        game_id = 1

        if game_id not in self.games:
            self.games[game_id] = Game()
            self.games[game_id].turn = 0
        if self.games[game_id].player[0] is None:
            log.debug("add first player")
            self.games[game_id].player[0] = Player(ws, 2)
        elif self.games[game_id].player[1] is None:
            log.debug("add second player")
            self.games[game_id].player[1] = Player(ws, 2)
            # both of players connected so we can start the round
            self.start_round(self.games[game_id])

        try:
            while True:
                msg = yield from ws.receive()
                log.info(msg.data)
                if msg.tp == MsgType.text:
                    if msg.data == 'close':
                        log.info("Error: {}".format(msg))
                        ws.close()
                    else:
                        self.execute_command(self.games[game_id], json.loads(msg.data))
                else:
                    log.info("Strange msg: {}".format(msg))
        except:
            pass
        finally:
            self.request.app['websockets'].remove(ws)
            log.info("Closing websocket connection")

        # for msg in ws:
        #     log.info(msg.data)
        #     if msg.tp == MsgType.text:
        #         if msg.data == 'close':
        #             log.info("Error: {}".format(msg))
        #             ws.close()
        #         else:
        #             self.execute_command(self.games[game_id], json.loads(msg.data))
        #     else:
        #         log.info("Strange msg: {}".format(msg))
        #
        # self.request.app['websockets'].remove(ws)
        #
        # log.info("Closing websocket connection")
        return ws
