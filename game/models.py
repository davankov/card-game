from random import randrange
class Player(object):
    def __init__(self, socket, money):
        self.websocket = socket
        self.cards = [Card(randrange(10)) for i in range(5)]
        self.money = money

class Game(object):
    def __init__(self):
        self.player = [None, None]
        self.Turn = 1


class Card(object):
    def __str__(self, *args, **kwargs):
        return super().__str__(*args, **kwargs)

    def __init__(self, id):
        self.ID = id
        if id == 0:
            self.attack = 3
            self.health = 2
            self.cost = 2
        if id == 1:
            self.attack = 1
            self.health = 2
            self.cost = 1
        if id == 2:
            self.attack = 2
            self.health = 1
            self.cost = 1
        if id == 3:
            self.attack = 1
            self.health = 4
            self.cost = 2
        if id == 4:
            self.attack = 3
            self.health = 3
            self.cost = 3
        if id == 5:
            self.attack = 4
            self.health = 2
            self.cost = 4
        if id == 6:
            self.attack = 4
            self.health = 4
            self.cost = 5
        if id == 7:
            self.attack = 5
            self.health = 3
            self.cost = 5
        if id == 8:
            self.attack = 6
            self.health = 5
            self.cost = 6
        if id == 9:
            self.attack = 9
            self.health = 3
            self.cost = 6
        self.params = [self.ID, self.attack, self.health, self.cost]
