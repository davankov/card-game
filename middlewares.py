import asyncio

@asyncio.coroutine
def db_handler(app, handler):
    @asyncio.coroutine
    def middleware(request):
        if request.path.startswith('/static/') or request.path.startswith('/_debugtoolbar'):
            response = yield from handler(request)
            return response

        request.db = app.db
        response = yield from  handler(request)
        return response
    return middleware
