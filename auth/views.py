import json
from time import time
from bson.objectid import ObjectId
import aiohttp_jinja2
from aiohttp_session import get_session
from aiohttp import web
from auth.models import User
from settings import log
import asyncio

def redirect(request, router_name):
    url = request.app.router[router_name].url()
    raise web.HTTPFound(url)


def set_session(session, user_id, request):
    session['user'] = str(user_id)
    session['last_visit'] = time()
    log.debug(session)
    redirect(request, 'main')


def convert_json(message):
    return json.dumps({'error': message})


class Register(web.View):

    @aiohttp_jinja2.template('auth/auth.html')
    @asyncio.coroutine
    def get(self):
        session = yield from get_session(self.request)
        log.debug(session.get('user'))
        if session.get('user'):
            redirect(self.request, 'main')
        return {'data': 'Please enter login or email'}

    @asyncio.coroutine
    def post(self):
        data = yield from self.request.post()
        user = User(self.request.db, data)
        result = yield from user.create_user()
        log.debug("create user: {}".format(result))
        if isinstance(result, ObjectId):
            # user created, everything is OK
            session = yield from get_session(self.request)
            set_session(session, str(result), self.request)
        else:
            # something went wrong
            return web.Response(content_type='application/json', text=convert_json("UserExists"))


class LogOut(web.View):
    
    @asyncio.coroutine
    def get(self, **kw):
        session = yield from get_session(self.request)
        if session.get('user'):
            log.debug("deleting user...")
            del session['user']
            log.debug("deleting user...OK")
            redirect(self.request, 'main')
        else:
            raise web.HTTPForbidden(body=b'Forbidden')


class LogIn(web.View):
    
    @asyncio.coroutine
    def post(self, **kw):
        data = yield from self.request.post()
        user = User(self.request.db, data)
        result = yield from user.check_user()
        if isinstance(result, dict):
            # user exists
            result = yield from user.check_user_password()
            if isinstance(result, dict):
                # password correct
                session = yield from get_session(self.request)
                set_session(session, str(result['_id']), self.request)
            else:
                return web.Response(content_type='application/json', text=convert_json("WrongPassword"))
        else:
            # user is not registered
            return web.Response(content_type='application/json', text=convert_json("NotExists"))
