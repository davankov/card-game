from bson import ObjectId

from settings import USER_COLLECTION
from settings import log
import asyncio


class User(object):

    def __init__(self, db, data, **kw):
        self.db = db
        self.collection = self.db[USER_COLLECTION]
        self.email = data.get('email')
        self.login = data.get('login')
        self.password = data.get('password')
        log.debug("user:\n - {}\n - {}\n - {}".format(self.login, self.email, self.password))

    @asyncio.coroutine
    def check_user(self, **kw):
        return self.collection.find_one({'login': self.login})

    @asyncio.coroutine
    def check_user_password(self, **kw):
        return self.collection.find_one({'login': self.login, 'password': self.password})

    @staticmethod
    @asyncio.coroutine
    def get_user(db, user_id):
        return db[USER_COLLECTION].find_one({'_id': ObjectId(user_id)})

    @asyncio.coroutine
    def create_user(self, **kw):
        user = yield from self.check_user()
        log.debug("create user...")
        if not user:
            result = yield from self.collection.insert({'email': self.email, 'login': self.login, 'password': self.password})
            log.debug("create user... OK")
        else:
            log.debug("create user... ERROR (User exists)")
            result = 'User exists'
        return result
