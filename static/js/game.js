window.onload = function() {
    var turn = -1;
    var player_id = -1;
    var attack_turn = false;
    var activated_card = -1;
    var texture = {};

    texture[0] = PIXI.Texture.fromImage('img/charmander.png');
    texture[1] = PIXI.Texture.fromImage('img/Froakie.png');
    texture[9] = PIXI.Texture.fromImage('img/Mewtwo.png');
    texture[3] = PIXI.Texture.fromImage('img/Lapras.png');
    texture[5] = PIXI.Texture.fromImage('img/lugia.gif');
    texture[2] = PIXI.Texture.fromImage('img/squirtle.png');
    texture[8] = PIXI.Texture.fromImage('img/megapokemon.png');
    texture[4] = PIXI.Texture.fromImage('img/typhlosion.png');
    var textureButton = PIXI.Texture.fromImage('img/card_back.png');
    var textureButtonDown = PIXI.Texture.fromImage('img/dragon.png');
    var textureButtonOver = PIXI.Texture.fromImage('img/pikachu.png');
    texture[6] = PIXI.Texture.fromImage('img/pikachu.png');
    texture[7] = PIXI.Texture.fromImage('img/dragon.png');
    var textureEndOfTurn = PIXI.Texture.fromImage('/img/End_Turn.png');
    var TYPE_ENEMY_CARD = 0;
    var TYPE_PLAYER_CARD = 1;
    var moneyText = new PIXI.Text('');
    var money = 0;

    function EndTurn() {
        Turn += 1;
    }

    function activateCard(card) {
        send({command: "ActivateCard", card: card.id});
        console.log("ACTIVATE");
        card.increase();
        activated_card = card.id;
    }

    function deactivateCard(card) {
        send({command: "DeactivateCard", card: card.id});
        console.log("DEACTIVATE");
        card.decrease();
        activated_card = -1;
    }


    function attack(card) {
        attacker = cards[activated_card];
        send({command: "Attack", attacker: attacker.id, defender: card.id, attack: attacker.attack});
        console.log("my card " ,attacker.id, "attack enemy card ", card.id);
        attacker_pos_x = attacker.container.position.x;
        attacker_pos_y = attacker.container.position.y;
        var move_card = new TWEEN.Tween(attacker.container.position)
            .to({'x': card.container.position.x, 'y': card.container.position.y}, 1000)
            .easing(TWEEN.Easing.Quadratic.InOut)
            .onComplete(function() {
                deactivateCard(attacker);
                attacker.onattack(card.attack);
                if (attacker.health > 0) {
                    var move_card_back = new TWEEN.Tween(attacker.container.position)
                        .to({'x': attacker_pos_x, 'y': attacker_pos_y}, 1000)
                        .easing(TWEEN.Easing.Quadratic.InOut);
                    move_card_back.start();
                }
                card.onattack(attacker.attack);
            });
        move_card.start();

    }

    function enemyAttack(defender, attacker, attack) {
        attacker_pos_x = defender.container.position.x;
        attacker_pos_y = defender.container.position.y;
        var move_card = new TWEEN.Tween(defender.container.position)
            .to({'x': attacker.container.position.x, 'y': attacker.container.position.y}, 1000)
            .easing(TWEEN.Easing.Quadratic.InOut)
            .onComplete(function() {
                //deactivateCard(attacker);
                defender.onattack(attacker.attack);
                if (defender.health > 0) {
                    var move_card_back = new TWEEN.Tween(defender.container.position)
                        .to({'x': attacker_pos_x, 'y': attacker_pos_y}, 1000)
                        .easing(TWEEN.Easing.Quadratic.InOut);
                    move_card_back.start();
                }
                attacker.onattack(defender.attack);
            });
        move_card.start();

    }

    function cardToTrash(card) {
        card.moveToTrash();
    }

    function enemyCardToField(card_id) {
        card = cards[card_id];
        enemyfieldcards[card_id] = card;
        card.decrease();
        card.active = true;
        var move_card = new TWEEN.Tween(card.container.position)
            .to({'x': card.small_x, 'y': height*0.33}, 1000)
            .easing(TWEEN.Easing.Quadratic.InOut);
        move_card.start();
    }
    
    function cardToField(card) {
        card.moveToField();
    }
    
    function enemyStopLookingAtCard(card_id) {
        cards[card_id].decrease();
    }
    
    function enemyLookingAtCard(card_id) {
        console.log(card_id);
        cards[card_id].increase();
    }
    
    function ParseData(data) {
        var data = JSON.parse(data);
        if (data.EnemyLookingAtCard) {
            enemyLookingAtCard(data.card - 5);
        }
        if (data.EnemyStopLookingAtCard) {
            enemyStopLookingAtCard(data.card - 5);
        }
        if (data.id >= 0) {
            console.log("my id is:", data.id);
            player_id = data.id;
        }
        if (data.StartRound) {
            money = data.money
            console.log(data.cards)
            cards[5].show(data.card0);
            cards[6].show(data.card1);
            cards[7].show(data.card2);
            cards[8].show(data.card3);
            cards[9].show(data.card4);
            turn = data.Turn;
            console.log(turn);
        }
        if (data.EnemyCardToField) {
            cards[data.card_id - 5].show(data.card)
            enemyCardToField(data.card_id - 5);
        }
        if (data.EnemyCardToTrash) {
            cardToTrash(cards[data.card - 5]);
        }
        if (data.Turn >= 0) {
            money += 2;
            moneyText.text = 'money: ' + money.toString();
            turn = data.Turn;
        }
        if (data.EnemyAttack) {
            console.log("enemy card ", data.attacker - 5, "attack my card ", data.defender + 5);
            enemyAttack(cards[data.attacker - 5], cards[data.defender + 5], data.attack);
        }
    }
    
    class Card {
    
        constructor(width, height, x, y, health, attack, card_id, type, cost) {
            this.scale = 1.3;
            this.container = new PIXI.Container();
            this.image = new PIXI.Sprite();
            this.healthtext = new PIXI.Text('');
            this.attacktext = new PIXI.Text('');
            this.moneytext = new PIXI.Text('');
            this.container.id = card_id;
            this.width = width;
            this.height = height;
            this.container.height = height;
            this.container.width = width;
            this.container.position.x = x;
            this.container.position.y = y;
            this.image.width = width;
            this.image.height = height;
            this.shift_x = (this.scale - 1) * (width / 2);
            this.shift_y =  (this.scale - 1) * (height / 2);
            this.attack = attack;
            this.health = health;
            this.id = card_id;
            this.active = false;
            this.cost = cost;

            if (type == TYPE_ENEMY_CARD) {
                this.player = false;
                this.enemy = true;
                this.image.texture = textureButton;
                console.log("create enemy card" + card_id.toString());
            }
            if (type == TYPE_PLAYER_CARD) {
                this.moneytext.text = cost.toString();
                this.healthtext.text = health.toString();
                this.attacktext.text = attack.toString();
                this.player = true;
                this.enemy = false;
                this.image.texture = textureButtonOver;
                console.log("create playr card" + card_id.toString());

            }
            this.container.addChild(this.image);
            console.log(typeof(this.moneytext));
            this.moneytext.x = 0;
            this.moneytext.y = this.height - 30;
            this.healthtext.x = this.width*0.45;
            this.healthtext.y = this.height - 30;
            this.attacktext.x = this.width*0.9;
            this.attacktext.y = this.height - 30;

            this.container.addChild(this.moneytext);
            this.container.addChild(this.healthtext);
            this.container.addChild(this.attacktext);
        }
        moveToField() {
            console.log("cardToField");
            this.field = true;
            var move_card = new TWEEN.Tween(this.container.position)
                .to({'x': this.small_x, 'y': height*0.50}, 1000)
                .easing(TWEEN.Easing.Quadratic.InOut);
            move_card.start();
            this.decrease();
//            this.image.position.x = this.small_x;
//            this.image.position.y = height*0.50;
        }

        increase() {
            if (this.container.width == this.width * this.scale) return;
            this.container.width = this.width * this.scale;
            this.container.height = this.height * this.scale;
            this.container.position.x -= this.shift_x;
            this.container.position.y -= this.shift_y;;
        }
    
        decrease() {
            if (this.container.width == this.width) return;
            this.container.width = this.width;
            this.container.height = this.height;
            this.container.position.x += this.shift_x;
            this.container.position.y += this.shift_y;
        }
    
        show (params) {
            this.image.texture = texture[params[0]];
            this.attack = params[1];
            this.health = params[2];
            this.cost = params[3];
            this.healthtext.text = this.health.toString();
            this.attacktext.text = this.attack.toString();
            this.moneytext.text = this.cost.toString();
            this.image_id = params[0]
        }

        close() {
            this.image.texture = textureButton;
        }

        onattack(attack) {
            this.health -= attack;
            this.healthtext.text = this.health.toString();
            if (this.health <= 0) {
                this.moveToTrash();
            }
        }

        moveToTrash() {
            this.container.removeChild(this.healthtext);
            this.container.removeChild(this.attacktext);
            this.container.removeChild(this.moneytext);
            this.close();
            var move_card = new TWEEN.Tween(this.container.position)
                .to({'x': 20, 'y': 250}, 1000)
                .easing(TWEEN.Easing.Quadratic.InOut);
            move_card.start();
        }
    }    
    
    var Interactivity = function(button) {
        button.interactive = true;
        button.buttonMode = true;
        button
            // set the mousedown and touchstart callback...
            .on('mousedown', onButtonDown)
            .on('touchstart', onButtonDown)
    
            // set the mouseup and touchend callback...
            .on('mouseup', onButtonUp)
            .on('touchend', onButtonUp)
            .on('mouseupoutside', onButtonUp)
            .on('touchendoutside', onButtonUp)
    
            // set the mouseover callback...
            .on('mouseover', onButtonOver)
    
            // set the mouseout callback...
            .on('mouseout', onButtonOut)
    };
    
    function onButtonDown() {
//        //this.isdown = true;
//        var card = cards[this.id];
//        //current_Turn = Turn;
//        //this.texture = textureButtonDown;
//        //this.alpha = 1;
//        if (card.player && !card.field) {
//            send({command: "CardToField", card: card.id});
//            cardToField(card);
//            //send({command: "EndTurn"})
//            //EndTurn();
//            return;
//        }
//        if (card.player && card.field && activated_card == -1) {
//            //send({command: "Attack", card: card.id, attack: card.attack});
//            //card.increase();
//            activateCard(card);
//            //send({command: "CardToTrash", card: card.id});
//            //cardToTrash(card);
//        }
//        if (card.player && card.field && activated_card ==  card.id) {
//            deactivateCard(card);
//        }
//        if (card.enemy && activated_card != -1) {
//            attack(card);
//            //AttackSecond(card);
//        }
////        if (card.enemy) {
////            return;
////        }
////
////        if (card.field) {
////            send({command: "CardToTrash", card: card.id});
////            cardToTrash(card);
////        }
        return;
    }
    
    function onButtonUp() {
        if (this.id == 2000) {
            if (turn % 2 != player_id) {
                turn += 1;
                console.log(turn, "TUUUUUUUUUUUUUURNNNNNNNNNN");
                send({command: "EndTurn", turn: turn});
            }
            return;
        }
        console.log(turn, player_id);
        if (turn == -1 || turn % 2 == player_id) return;
        var card = cards[this.id];
        //current_Turn = Turn;
        //this.texture = textureButtonDown;
        //this.alpha = 1;
        if (card.player && !card.field && card.cost <= money) {
            money -= card.cost;
            moneyText.text = 'money: ' + money.toString();
            send({command: "CardToField", card: card.id});
            console.log("CAAAAAAARRRRRRDDDDDTOOFIELD " + card.id.toString());
            cardToField(card);
            //send({command: "EndTurn"})
            //EndTurn();
            return;
        }
        if (card.player && card.field && activated_card == -1) {
            //send({command: "Attack", card: card.id, attack: card.attack});
            //card.increase();
            activateCard(card);
            //send({command: "CardToTrash", card: card.id});
            //cardToTrash(card);
            return;
        }
        if (card.player && card.field && activated_card ==  card.id) {
            deactivateCard(card);
            return;
        }
        if (card.enemy && activated_card != -1) {
            attack(card);
            return;
            //AttackSecond(card);
        }
    }
    
    function onButtonOver() {
        if (this.id == 2000) return;
        var card = cards[this.id];
        console.log("get " + card.id.toString() + " card on id " + this.id.toString());
        if (card.enemy || activated_card != -1) return;
        if (card.field) return;
        card.increase();
        send({command: "LookingAtCard", card: card.id});
    
    }
    
    function onButtonOut() {
        if (this.id == 2000) return;
        var card = cards[this.id];

        if (card.enemy) return;
    
        if (card.field) return;
        card.decrease();
        send({command: "StopLookingAtCard", card: card.id});
    }
    
    function send(message) {
        message["id"] = player_id;
        console.log(JSON.stringify(message));
        socket.send(JSON.stringify(message));
    }
    
    function animate() {
        requestAnimationFrame(animate);
        TWEEN.update();
    
        renderer.render(stage);
    }
    
    function draw() {
    
        //drow cards, counts of money and endButton
        var container = new PIXI.Container();
        stage.addChild(container);
    
        container.x = 10;
        container.y = 10;
    
        var leftPadding = width * 0.27;
        var horizontalPadding = width * 0.02;
        var cardWidth = width * 0.07;
        var topPadding = height * 0.05;
        var cardHeight = cardWidth * 1.5;
    
        var second_container = new PIXI.Container();
        stage.addChild(second_container);
        for (var i = 0; i < 5; i++) {
            var id = i;
            var card = new Card(cardWidth, cardHeight,
                    leftPadding + i * (cardWidth + horizontalPadding), topPadding, i, i + 1,
                    id, TYPE_ENEMY_CARD, i);
            Interactivity(card.container);
            container.addChild(card.container);
            cards[id] = card;
            console.log("add " + card.id.toString() + " card on id " + id.toString());
        }

    
        for (var i = 0; i < 5; i++) {
            var id = i + 5;
            var card = new Card(cardWidth, cardHeight,
                    leftPadding + i * (cardWidth + horizontalPadding), height - topPadding - cardHeight, i, i + 1,
                    id, TYPE_PLAYER_CARD, i);
            Interactivity(card.container);
            card.image.texture = textureButton;
            container.addChild(card.container);
            cards[id] = card;
            console.log("add " + card.id.toString() + " card on id " + id.toString());
        }

        moneyText.text = 'money: ' + money.toString();
        moneyText.x = width*0.75;
        moneyText.y = height*0.5;

        var style = {
            fontFamily : 'Arial',
            fontSize : 50,
            fontStyle : 'italic',
            fontWeight : 'bold',
            fill : '#F7EDCA'
        };
        moneyText.style = style;
        container.addChild(moneyText);

        var endButton = new PIXI.Sprite();
        endButton.texture =  textureEndOfTurn;
        endButton.id = 2000;
        endButton.position.x = width*0.75;
        endButton.position.y = height*0.7;
        endButton.width = 50;
        endButton.height =75;
        Interactivity(endButton);
        container.addChild(endButton);

    }
    // global variables
    var cards = {};
    var enemyfieldcards = {};
    var socket;
    try {
        socket = new WebSocket('ws://' + window.location.host + '/ws');
    } catch(err) {
        socket = new WebSocket('wss://' + window.location.host + '/ws');
    }
    var mainDiv = document.getElementById("main");
    var width = window.screen.width * 0.9;
    var height = window.screen.height * 0.8;
    mainDiv.style.width = width;
    mainDiv.style.height = height;
    mainDiv.style.margin = "0 auto";
    document.body.style.backgroundColor = "#1099bb";
    var renderer = PIXI.autoDetectRenderer(width, height, {backgroundColor: "0x1099bb"});
    var stage = new PIXI.Container();

    // draw game
    mainDiv.appendChild(renderer.view);
    draw();

    socket.onmessage = function(event) {
        console.log(event.data);
        ParseData(event.data);
    };

    requestAnimationFrame(animate);
};