$(document).ready(function() {
    $('.tab a').on("click", function (e) {
        console.log("tab click");
        e.preventDefault();

        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');

        target = $(this).attr('href');

        $('.tab-content > div').not(target).hide();

        $(target).fadeIn(600);

    });

    $('#login_form').submit(function () {
        // prevent refreshing page onSubmit
        return false;
    });

    $('#register_form').submit(function () {
        // prevent refreshing page onSubmit
        return false;
    });

    $('#register_btn').click(function(){
        var login = $('#username').val(),
            password = $('#password').val(),
            email = $('#email').val();
        console.log("register button click");
        if(login && password && email){
            $.post('/register', {'login': login, 'password': password, "email": email}, function(data) {
                console.log(data) ;
                if (!data.error) {
                    window.location.href = '/';
                    return;
                }
                if (data.error.localeCompare('UserExists') == 0) {
                    $('#error_message').text("User Already Exists");
                }
            });
        }
    });
    
    $('#login_btn').click(function() {
        var login = $('#l_username').val(),
            password = $('#l_password').val();
        console.log("login button click");
        if(login && password){
            $.post('/login', {'login': login, 'password': password}, function(data) {
                console.log(data);
                if (!data.error) {
                    window.location.href = '/';
                    return;
                }
                if (data.error.localeCompare('WrongPassword') == 0) {
                    console.log('Wrong Password');
                    $('#l_error_message').text("Wrong Password");
                } else if (data.error.localeCompare("NotExists") == 0) {
                    console.log("Not Exists");
                    $('#l_error_message').text("User doesn't exist");
                }
            });
        }
    });

    $('.form').find('input, textarea').on('keyup blur focus', function (e) {

        var $this = $(this),
            label = $this.prev('label');

        if (e.type === 'keyup') {
            if ($this.val() === '') {
                label.removeClass('active highlight');
            } else {
                label.addClass('active highlight');
            }
        } else if (e.type === 'blur') {
            if ($this.val() === '') {
                label.removeClass('active highlight');
            } else {
                label.removeClass('highlight');
            }
        } else if (e.type === 'focus') {

            if ($this.val() === '') {
                label.removeClass('highlight');
            }
            else if ($this.val() !== '') {
                label.addClass('highlight');
            }
        }

    });
});